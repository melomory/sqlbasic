﻿namespace SQLite
{
	public static class Queries
	{
		/// <summary>
		/// Написать запрос, который выводит только недоставленные заказы из таблицы Orders (т.е. в колонке ShippedDate нет значения даты доставки). 
		/// Запрос должен высвечивать только колонки:
		/// • OrderID;
		/// • ShippedDate.
		/// Для колонки ShippedDate вместо значений NULL выводить строку ‘Not Shipped’ – для этого использовать системную функцию CASЕ
		/// </summary>
		public static string Query2_1_2 =>
			"SELECT OrderID," +
            "       CASE" +
            "            WHEN ShippedDate IS NULL" +
            "                 THEN 'Not Shipped' " +
            "            ELSE ShippedDate " +
            "       END AS ShippedDate" +
            "  FROM [Orders]" +
			" WHERE ShippedDate IS NULL;";
		
		/// <summary>
		/// Выбрать из таблицы Customers всех заказчиков, проживающих в USA и Canada. 
		/// Запрос сделать с только помощью оператора IN. 
		/// Высвечивать колонки с именем пользователя и названием страны в результатах запроса. 
		/// Упорядочить результаты запроса по имени заказчиков и по месту проживания.
		/// </summary>
		public static string Query2_2_1 =>
			"SELECT CompanyName," +
            "       Country" +
            "  FROM [Customers]" +
            " WHERE Country IN('USA', 'Canada')" +
            " ORDER BY CompanyName ASC, Country ASC;";

		/// <summary>
		/// Выбрать из таблицы Customers всех заказчиков, не проживающих в USA и Canada. 
		/// Запрос сделать с помощью оператора IN. 
		/// Высвечивать колонки с именем пользователя и названием страны в результатах запроса. 
		/// Упорядочить результаты запроса по имени заказчиков.
		/// </summary>
		public static string Query2_2_2 =>
			"SELECT CompanyName," +
			"       Country" +
			"  FROM [Customers]" +
			" WHERE Country NOT IN('USA', 'Canada')" +
			" ORDER BY CompanyName ASC";

		/// <summary>
		/// Выбрать из таблицы Customers все страны, в которых проживают заказчики. 
		/// Страна должна быть упомянута только один раз. 
		/// Cписок стран должен быть отсортирован по убыванию. 
		/// Не использовать предложение GROUP BY. 
		/// Высвечивать только одну колонку в результатах запроса.
		/// </summary>
		public static string Query2_2_3 =>
			"SELECT DISTINCT Country" +
			"  FROM [Customers]" +
			" ORDER BY Country DESC;";

		/// <summary>
		/// Выбрать все заказы (OrderID) из таблицы Order Details (заказы не должны 
		/// повторяться), где встречаются продукты с количеством от 3 до 10 включительно – это 
		/// колонка Quantity в таблице Order Details. 
		/// Использовать оператор BETWEEN. 
		/// Запрос должен высвечивать колонки OrderID и Quantity.
		/// </summary>
		public static string Query2_3_1 =>
			"SELECT DISTINCT OrderID," +
            "       Quantity" +
            "  FROM [Order Details]" +
            " WHERE Quantity BETWEEN 3 AND 10;";

		/// <summary>
		/// Выбрать всех заказчиков из таблицы Customers, у которых название страны начинается на буквы из диапазона b и g. 
		/// Использовать оператор BETWEEN. 
		/// Проверить, что в результаты запроса попадает Germany. 
		/// Запрос должен высвечивать только колонки
		/// • CustomerID;
		/// • Country.
		/// Результат запроса должен быть отсортирован по Country.
		/// </summary>
		public static string Query2_3_2 =>
			"SELECT CustomerID," +
            "       Country" +
            "  FROM [Customers]" +
            " WHERE SUBSTR(Country, 1, 1) BETWEEN 'B' AND 'G'" +
            " ORDER BY Country ASC;";

		/// <summary>
		/// Выбрать всех заказчиков из таблицы Customers, у которых название страны начинается на буквы из диапазона b и g. 
		/// Не использовать оператор BETWEEN. 
		/// Запрос должен высвечивать только колонки
		/// • CustomerID;
		/// • Country.
		/// Результат запроса должен быть отсортирован по Country.
		/// </summary>
		public static string Query2_3_3 =>
			"SELECT CustomerID," +
            "       Country" +
            "  FROM [Customers]" +
            " WHERE Country >= 'B' AND Country< 'H'" +
            " ORDER BY Country ASC;";
		
		/// <summary>
		/// В таблице Products найти все продукты (колонка ProductName), где встречается 
		/// подстрока 'chocolade'. Известно, что в подстроке 'chocolade' может быть изменена одна 
		/// буква 'c' в середине - найти все продукты, которые удовлетворяют этому условию. 
		/// Подсказка: результаты запроса должны высвечивать 2 строки.
		/// Запрос должен высвечивать только колонку ProductName
		/// </summary>
		public static string Query2_4_1 =>
			"SELECT ProductName" +
            "  FROM [Products]" +
            " WHERE ProductName LIKE '%cho_olade%';";

		/// <summary>
		/// Для формирования алфавитного указателя Employees высветить из таблицы 
		/// Employees список только тех букв алфавита, с которых начинаются фамилии Employees (колонка LastName) из этой таблицы. 
		/// Алфавитный список должен быть отсортирован по возрастанию
		/// Использовать функцию substr(string, start, length)
		/// </summary>
		public static string Query2_4_2 =>
			"SELECT DISTINCT SUBSTR(LastName, 1, 1) AS AlphabetPointer" +
            "  FROM [Employees]" +
            " ORDER BY AlphabetPointer ASC;";
		
		/// <summary>
		/// Определить продавцов, которые обслуживают регион 'Western' (таблица Region). 
		/// Результаты запроса должны высвечивать поля: 
		/// • 'LastName' продавца;
		/// • название обслуживаемой территории ('TerritoryDescription' из таблицы Territories).
		/// Запрос должен использовать JOIN в предложении FROM.
		/// Для определения связей между таблицами Employees и Territories надо использовать графические диаграммы для базы Northwind.
		/// </summary>
		public static string Query2_5_1 =>
			"SELECT [Employees].LastName," +
            "       [Territories].TerritoryDescription" +
            "  FROM [Employees]" +
            " INNER JOIN [EmployeeTerritories]" +
            "    ON [Employees].EmployeeID = [EmployeeTerritories].EmployeeID" +
            " INNER JOIN [Territories]" +
            "    ON [EmployeeTerritories].TerritoryID = [Territories].TerritoryID" +
            " INNER JOIN [Regions]" +
            "    ON [Territories].RegionID = [Regions].RegionID" +
            " WHERE [Regions].RegionDescription = 'Western';";
		
		/// <summary>
		/// Высветить в результатах запроса имена всех заказчиков (CompanyName) из таблицы Customers и суммарное количество их заказов из таблицы Orders. 
		/// Принять во внимание, что у некоторых заказчиков нет заказов, но они также должны быть выведены в результатах запроса. 
		/// Запрос должен высвечивать только колонки CompanyName и OrdersCount
		/// Упорядочить результаты запроса по возрастанию количества заказов.
		/// </summary>
		public static string Query2_5_2 =>
			"SELECT [Customers].CompanyName," +
            "       COUNT([Orders].CustomerID) AS OrdersCount" +
            "  FROM [Customers]" +
            "  LEFT JOIN [Orders]" +
            "    ON [Customers].CustomerID = [Orders].CustomerID" +
            " GROUP BY [Customers].CompanyName" +
            " ORDER BY OrdersCount";

		/// <summary>
		/// Высветить всех поставщиков (колонка CompanyName в таблице Suppliers), у 
		/// которых нет хотя бы одного продукта на складе (UnitsInStock в таблице Products равно 0).
		/// </summary>
		public static string Query2_5_3 =>
			"SELECT [Suppliers].CompanyName" +
            "  FROM [Suppliers]" +
            " INNER JOIN [Products]" +
            "    ON [Suppliers].SupplierID = [Products].SupplierID" +
            " WHERE [Products].UnitsInStock = 0";

		/// <summary>
		/// Найти общую сумму всех заказов из таблицы Order Details с учетом количества закупленных товаров и скидок по ним. 
		/// Результат округлить до сотых. Скидка (колонка Discount) составляет процент из стоимости для данного товара. 
		/// Результатом запроса должна быть одна запись с одной колонкой
		/// </summary>
		public static string Query3_1_1 =>
			"SELECT ROUND" +
            "         (SUM" +
			"           (UnitPrice * Quantity * (1 - Discount)" +
            "          )," +
            "          2" +
            "         ) AS Total" +
            "  FROM [Order Details]";

		/// <summary>
		/// По таблице Orders найти количество заказов, которые еще не были  доставлены (т.е. в колонке ShippedDate нет значения даты доставки). 
		/// Использовать при этом запросе только оператор COUNT. 
		/// Не использовать предложения WHERE и GROUP.
		/// Результатом запроса должна быть одна запись с одной колонкой
		/// </summary>
		public static string Query3_1_2 =>
			"SELECT COUNT() - COUNT(ShippedDate)" +
			"  FROM [Orders]";

		/// <summary>
		/// По таблице Orders найти количество различных покупателей (CustomerID), сделавших заказы. 
		/// Использовать функцию COUNT и не использовать предложения WHERE и GROUP.
		/// </summary>
		public static string Query3_1_3 =>
			"SELECT COUNT(DISTINCT CustomerID)" +
            "  FROM [Orders]";

		/// <summary>
		/// По таблице Orders найти количество заказов, оформленных каждым продавцом. 
		/// Заказ для указанного продавца – это любая запись в таблице Orders, где в колонке 
		/// EmployeeID задано значение для данного продавца.В результатах запроса надо высвечивать колонку с именем продавца (Должно 
		/// высвечиваться имя, полученное конкатенацией LastName & FirstName. Эта строка 
		/// LastName & FirstName должна быть получена отдельным запросом в колонке 
		/// основного запроса. Также основной запрос должен использовать группировку 
		/// по EmployeeID.) с названием колонки ‘Seller’ и колонку c количеством заказов 
		/// высвечивать с названием 'Amount'. 
		/// Результаты запроса должны быть упорядочены по убыванию количества заказов.
		/// Для конкатенации строк в SQLite использовать оператор ||
		/// </summary>
		public static string Query3_2_2 =>
			"SELECT (SELECT LastName || ' ' || FirstName" +
            "          FROM [Employees]" +
            "         WHERE [Orders].EmployeeID = [Employees].EmployeeID" +
            "       ) AS Seller," +
            "       COUNT ([Orders].EmployeeID) AS Amount" +
            "  FROM [Orders]" +
            " GROUP BY [Orders].EmployeeID" +
            " ORDER BY Amount DESC;";

		/// <summary>
		/// Найти покупателей и продавцов, которые живут в одном городе. 
		/// Если в городе живут только продавцы или только покупатели, то информация о 
		/// таких покупателя и продавцах не должна попадать в результирующий набор. 
		/// В результатах запроса необходимо вывести следующие заголовки для результатов запроса: 
		/// • ‘Person’;
		/// • ‘Type’ (здесь надо выводить строку ‘Customer’ или ‘Seller’ в зависимости от типа записи);
		/// • ‘City’. 
		/// Отсортировать результаты запроса по колонке ‘City’ и по ‘Person’.
		/// </summary>
		public static string Query3_2_4 =>
			"SELECT C.ContactName AS Person," +
            "       'Customer' AS Type," +
            "       C.City" +
            "  FROM [Customers] C" +
            " WHERE EXISTS" +
            "       (" +
            "         SELECT 1 " +
            "           FROM [Employees] E " +
            "          WHERE E.City = C.City" +
            "       )" +
            " UNION ALL " +
            "SELECT E.LastName AS Person," +
            "       'Seller' AS Type," +
            "        E.City" +
            "  FROM [Employees] E" +
            " WHERE EXISTS" +
            "       (" +
            "         SELECT 1 " +
            "           FROM [Customers] C " +
            "          WHERE C.City = E.City" +
            "       )" +
			"ORDER BY City ASC, Person ASC;";

		/// <summary>
		/// Найти всех покупателей, которые живут в одном городе. 
		/// В запросе использовать соединение таблицы Customers c собой -
		/// самосоединение. Высветить колонки CustomerID и City. 
		/// Запрос не должен высвечивать дублируемые записи. 
		/// </summary>
		public static string Query3_2_5 =>
			"SELECT DISTINCT C1.CustomerID," +
            "       C1.City" +
            "  FROM [Customers] C1" +
            " INNER JOIN [Customers] C2" +
            "    ON C1.City = C2.City AND (C1.CustomerID<> C2.CustomerID)" +
            " ORDER BY C1.City;";

		/// <summary>
		/// По таблице Employees найти для каждого продавца его руководителя, т.е. кому он делает репорты (ReportsTo). 
		/// Высветить колонки с именами 'User Name' и 'Boss'. В колонках должны быть высвечены имена из колонки LastName.
		/// В результате должны быть только те сотрудники, у которых есть руководитель 
		/// </summary>
		public static string Query3_2_6 =>
			"SELECT E1.LastName AS UserName," +
            "       E2.LastName AS Boss" +
            "  FROM [Employees] E1" +
            " INNER JOIN [Employees] E2" +
            "    ON E2.EmployeeID = E1.ReportsTo;";

		/// <summary>
		/// Написать запрос, который добавляет новый заказ в таблицу Orders, используя VALUES
		/// </summary>
		public static string Query4_1_1_1 =>
			"INSERT INTO [Orders] (CustomerID, EmployeeID, OrderDate)" +
            "VALUES ('ALFKI', 5, DATETIME('now'));";

		/// <summary>
		/// Написать запрос, который добавляет новый заказ в таблицу Orders, используя SELECT
		/// </summary>
		public static string Query4_1_1_2 =>
			"INSERT INTO [Orders] (CustomerID, EmployeeID)" +
            "SELECT CustomerID, EmployeeID" +
            "  FROM [Orders]" +
            " WHERE [Orders].OrderID = 11003;";
	}
}                                                                                                                                      