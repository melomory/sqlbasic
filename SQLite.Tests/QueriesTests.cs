using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.Common;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Data.Sqlite;
using Newtonsoft.Json;
using NUnit.Framework;

namespace SQLite.Tests
{
	public class QueriesTests
	{
		private DbConnection _dbConnection;

		[SetUp]
		public async Task SetupAsync()
		{
			_dbConnection = new SqliteConnection("Filename=Northwind.db");
			await _dbConnection.OpenAsync();

			await _dbConnection.BeginTransactionAsync();
		}

		[TearDown]
		public async Task TearDown()
		{
			await _dbConnection.CloseAsync();
			await _dbConnection.DisposeAsync();
		}

		[Test]
		public async Task Query2_1_2()
		{
			StringAssert.Contains("CASE", Queries.Query2_1_2.ToUpper());

			var selectCommand = _dbConnection.CreateCommand();
			selectCommand.CommandText = Queries.Query2_1_2;

			var reader = await selectCommand.ExecuteReaderAsync();
			var columns = reader.GetColumnSchema();

			CollectionAssert.AreEquivalent(new []{"OrderID", "ShippedDate"}, columns.Select(c => c.ColumnName));

			while (await reader.ReadAsync())
			{
				StringAssert.AreEqualIgnoringCase("Not Shipped", reader["ShippedDate"].ToString());
			}
		}

		[Test]
		public async Task Query2_2_1()
		{
			StringAssert.Contains("IN", Queries.Query2_2_1.ToUpper());

			var selectCommand = _dbConnection.CreateCommand();
			selectCommand.CommandText = Queries.Query2_2_1;

			var reader = await selectCommand.ExecuteReaderAsync();
			var columns = reader.GetColumnSchema();

			CollectionAssert.AreEquivalent(new []{"CompanyName", "Country"}, columns.Select(c => c.ColumnName));

			var firstRead = true;
			string lastCompanyName = String.Empty;
			while (await reader.ReadAsync())
			{
				if (firstRead)
				{
					Assert.AreEqual("Bottom-Dollar Markets", reader["CompanyName"]);
					firstRead = false;
				}
				CollectionAssert.Contains(new[] {"USA", "Canada"}, reader["Country"]);
				lastCompanyName = reader["CompanyName"].ToString();
			}
			StringAssert.AreEqualIgnoringCase("White Clover Markets", lastCompanyName);
		}

		[Test]
		public async Task Query2_2_2()
		{
			StringAssert.Contains("IN", Queries.Query2_2_2.ToUpper());

			var selectCommand = _dbConnection.CreateCommand();
			selectCommand.CommandText = Queries.Query2_2_2;

			var reader = await selectCommand.ExecuteReaderAsync();
			var columns = reader.GetColumnSchema();

			CollectionAssert.AreEquivalent(new []{"CompanyName", "Country"}, columns.Select(c => c.ColumnName));

			var firstRead = true;
			string lastCompanyName = String.Empty;
			while (await reader.ReadAsync())
			{
				if (firstRead)
				{
					StringAssert.AreEqualIgnoringCase("Alfreds Futterkiste", reader["CompanyName"].ToString());
					firstRead = false;
				}
				CollectionAssert.DoesNotContain(new[] {"USA", "Canada"}, reader["Country"]);
				lastCompanyName = reader["CompanyName"].ToString();
			}
			StringAssert.AreEqualIgnoringCase("Wolski  Zajazd", lastCompanyName);
		}

		[Test]
		public async Task Query2_2_3()
		{
			StringAssert.DoesNotContain("GROUP BY", Queries.Query2_2_3.ToUpper());

			var selectCommand = _dbConnection.CreateCommand();
			selectCommand.CommandText = Queries.Query2_2_3;

			var reader = await selectCommand.ExecuteReaderAsync();
			var columns = reader.GetColumnSchema();

			CollectionAssert.AreEquivalent(new []{"Country"}, columns.Select(c => c.ColumnName));

			var firstRead = true;
			var countries = new List<string>();
			string country = String.Empty;
			while (await reader.ReadAsync())
			{
				country = reader["Country"].ToString();
				if (firstRead)
				{
					StringAssert.AreEqualIgnoringCase("Venezuela", country);
					firstRead = false;
				}
				CollectionAssert.DoesNotContain(countries, country);
				countries.Add(country);
			}
			StringAssert.AreEqualIgnoringCase("Argentina", country);
		}

		[Test]
		public async Task Query2_3_1()
		{
			var selectCommand = _dbConnection.CreateCommand();
			selectCommand.CommandText = Queries.Query2_3_1;

			var reader = await selectCommand.ExecuteReaderAsync();
			var columns = reader.GetColumnSchema();

			CollectionAssert.AreEquivalent(new []{"OrderID", "Quantity"}, columns.Select(c => c.ColumnName));

			while (await reader.ReadAsync())
			{
				Assert.LessOrEqual(3, int.Parse(reader["Quantity"].ToString()!));
				Assert.GreaterOrEqual(10, int.Parse(reader["Quantity"].ToString()!));
			}
		}

		[Test]
		public async Task Query2_3_2()
		{
			StringAssert.Contains("BETWEEN", Queries.Query2_3_2.ToUpper());
			
			var selectCommand = _dbConnection.CreateCommand();
			selectCommand.CommandText = Queries.Query2_3_2;

			var reader = await selectCommand.ExecuteReaderAsync();
			var columns = reader.GetColumnSchema();

			CollectionAssert.AreEquivalent(new []{"CustomerID", "Country"}, columns.Select(c => c.ColumnName));

			var firstRead = true;
			string country = String.Empty;
			while (await reader.ReadAsync())
			{
				country = reader["Country"].ToString();
				if (firstRead)
				{
					StringAssert.AreEqualIgnoringCase("Belgium", country);
					firstRead = false;
				}
			}
			StringAssert.AreEqualIgnoringCase("Germany", country);
		}

		[Test]
		public async Task Query2_3_3()
		{
			StringAssert.DoesNotContain("BETWEEN", Queries.Query2_2_3.ToUpper());
			
			var selectCommand = _dbConnection.CreateCommand();
			selectCommand.CommandText = Queries.Query2_3_3;

			var reader = await selectCommand.ExecuteReaderAsync();
			var columns = reader.GetColumnSchema();

			CollectionAssert.AreEquivalent(new []{"CustomerID", "Country"}, columns.Select(c => c.ColumnName));

			var firstRead = true;
			string country = String.Empty;
			while (await reader.ReadAsync())
			{
				country = reader["Country"].ToString();
				if (firstRead)
				{
					StringAssert.AreEqualIgnoringCase("Belgium", country);
					firstRead = false;
				}
			}
			StringAssert.AreEqualIgnoringCase("Germany", country);
		}

		[Test]
		public async Task Query2_4_1()
		{
			StringAssert.Contains("LIKE", Queries.Query2_4_1.ToUpper());
			
			var selectCommand = _dbConnection.CreateCommand();
			selectCommand.CommandText = Queries.Query2_4_1;

			var reader = await selectCommand.ExecuteReaderAsync();
			var columns = reader.GetColumnSchema();

			CollectionAssert.AreEquivalent(new []{"ProductName"}, columns.Select(c => c.ColumnName));

			int count = 0;
			while (await reader.ReadAsync())
			{
				count++;
			}
			Assert.AreEqual(2, count);
		}

		[Test]
		public async Task Query2_4_2()
		{
			StringAssert.Contains("SUBSTR", Queries.Query2_4_2.ToUpper());

			var selectCommand = _dbConnection.CreateCommand();
			selectCommand.CommandText = Queries.Query2_4_2;

			var reader = await selectCommand.ExecuteReaderAsync();

			int count = 0;
			char letter = char.MinValue;
			var letters = new []{'B', 'C', 'D', 'F', 'K', 'L', 'P', 'S'};
			while (await reader.ReadAsync())
			{
				letter = reader.GetChar(0);
				Assert.AreEqual(letters[count], letter);
				count++;
			}
			Assert.AreEqual(8, count);
			Assert.AreEqual('S', letter);
		}

		[Test]
		public async Task Query2_5_1()
		{
			StringAssert.Contains("JOIN", Queries.Query2_5_1.ToUpper());

			var selectCommand = _dbConnection.CreateCommand();
			selectCommand.CommandText = Queries.Query2_5_1;

			var reader = await selectCommand.ExecuteReaderAsync();
			var columns = reader.GetColumnSchema();

			CollectionAssert.AreEquivalent(new []{"LastName", "TerritoryDescription"}, columns.Select(c => c.ColumnName));

			var descriptions = new List<string>(new []
			{
				"Hoffman Estates",
				"Chicago",
				"Denver",
				"Colorado Springs",
				"Phoenix",
				"Scottsdale",
				"Santa Monica",
				"Menlo Park",
				"San Francisco",
				"Campbell",
				"Santa Clara",
				"Santa Cruz",
				"Bellevue",
				"Redmond",
				"Seattle"
			});
			while (await reader.ReadAsync())
			{
				var description = reader["TerritoryDescription"].ToString();
				CollectionAssert.Contains(descriptions, description);
				Assert.IsTrue(descriptions.Remove(description));
			}
			CollectionAssert.IsEmpty(descriptions);
		}

		[Test]
		public async Task Query2_5_2()
		{
			StringAssert.Contains("JOIN", Queries.Query2_5_2.ToUpper());
			
			var selectCommand = _dbConnection.CreateCommand();
			selectCommand.CommandText = Queries.Query2_5_2;

			var reader = await selectCommand.ExecuteReaderAsync();
			var columns = reader.GetColumnSchema();

			CollectionAssert.AreEquivalent(new []{"CompanyName", "OrdersCount"}, columns.Select(c => c.ColumnName));

			int count = 0;
			bool firstRead = true;
			var ordersCount = 0;
			while (await reader.ReadAsync())
			{
				ordersCount = int.Parse(reader["OrdersCount"].ToString()!);
				if (firstRead)
				{
					Assert.AreEqual(0, ordersCount);
					firstRead = false;
				}

				count++;
			}
			Assert.AreEqual(91, count);
			Assert.AreEqual(31, ordersCount);
		}

		[Test]
		public async Task Query2_5_3()
		{
			StringAssert.Contains("JOIN", Queries.Query2_5_3.ToUpper());
			
			var selectCommand = _dbConnection.CreateCommand();
			selectCommand.CommandText = Queries.Query2_5_3;

			var reader = await selectCommand.ExecuteReaderAsync();
			var columns = reader.GetColumnSchema();

			CollectionAssert.AreEquivalent(new []{"CompanyName"}, columns.Select(c => c.ColumnName));

			var suppliers = new List<string>();
			while (await reader.ReadAsync())
			{
				suppliers.Add(reader.GetString(0));
			}

			var expected = new[]
			{
				"New Orleans Cajun Delights",
				"Pavlova, Ltd.",
				"Plutzer Lebensmittelgroßmärkte AG",
				"Formaggi Fortini s.r.l.",
				"G'day, Mate"
			};

			CollectionAssert.AreEquivalent(expected, suppliers);
		}

		[Test]
		public async Task Query3_1_1()
		{
			StringAssert.DoesNotMatch(@"\d\d+", Queries.Query3_1_1);
			
			var selectCommand = _dbConnection.CreateCommand();
			selectCommand.CommandText = Queries.Query3_1_1;

			var result = (decimal?)(double?)await selectCommand.ExecuteScalarAsync();
			Assert.AreEqual(1265793.04m, result);
		}

		[Test]
		public async Task Query3_1_2()
		{
			StringAssert.DoesNotMatch(@"\d+", Queries.Query3_1_2);
			StringAssert.DoesNotContain("WHERE", Queries.Query3_1_2);
			StringAssert.DoesNotContain("GROUP", Queries.Query3_1_2);
			
			var selectCommand = _dbConnection.CreateCommand();
			selectCommand.CommandText = Queries.Query3_1_2;

			var result = (long?)await selectCommand.ExecuteScalarAsync();
			Assert.AreEqual(21, result);
		}

		[Test]
		public async Task Query3_1_3()
		{
			StringAssert.DoesNotMatch(@"\d+", Queries.Query3_1_3);
			StringAssert.DoesNotContain("WHERE", Queries.Query3_1_3);
			StringAssert.DoesNotContain("GROUP", Queries.Query3_1_3);
			
			var selectCommand = _dbConnection.CreateCommand();
			selectCommand.CommandText = Queries.Query3_1_3;

			var result = (long?)await selectCommand.ExecuteScalarAsync();
			Assert.AreEqual(89, result);
		}

		[Test]
		public async Task Query3_2_2()
		{
			var selectCommand = _dbConnection.CreateCommand();
			selectCommand.CommandText = Queries.Query3_2_2;

			var reader = await selectCommand.ExecuteReaderAsync();
			var columns = reader.GetColumnSchema();
			CollectionAssert.AreEquivalent(new []{"Seller", "Amount"}, columns.Select(c => c.ColumnName));

			var type = new [] {new {Seller = "", Amount = 0}}.GetType();
			var expectedSellers = JsonConvert.DeserializeObject(await File.ReadAllTextAsync(Path.Combine(TestContext.CurrentContext.TestDirectory, "Results", "Query3_2_2.json")), type);
			var sellers = new List<dynamic>();
			while (await reader.ReadAsync())
			{
				var seller = reader["Seller"].ToString();
				var amount = int.Parse(reader["Amount"].ToString()!);
				sellers.Add(new {Seller = seller, Amount = amount});
			}

			CollectionAssert.AreEqual(expectedSellers as IEnumerable, sellers);
		}

		[Test]
		public async Task Query3_2_4()
		{
			var selectCommand = _dbConnection.CreateCommand();
			selectCommand.CommandText = Queries.Query3_2_4;

			var reader = await selectCommand.ExecuteReaderAsync();
			var columns = reader.GetColumnSchema();
			CollectionAssert.AreEquivalent(new []{"Person", "Type", "City"}, columns.Select(c => c.ColumnName));

			var type = new [] {new {Person = "", Type = "", City = ""}}.GetType();
			var expectedPersons = JsonConvert.DeserializeObject(await File.ReadAllTextAsync(Path.Combine(TestContext.CurrentContext.TestDirectory, "Results", "Query3_2_4.json")), type);
			var persons = new List<dynamic>();
			while (await reader.ReadAsync())
			{
				var person = reader["Person"].ToString();
				var personType = reader["Type"].ToString();
				var city = reader["City"].ToString();
				persons.Add(new {Person = person, Type = personType, City = city});
			}

			CollectionAssert.AreEqual(expectedPersons as IEnumerable, persons);
		}

		[Test]
		public async Task Query3_2_5()
		{
			var selectCommand = _dbConnection.CreateCommand();
			selectCommand.CommandText = Queries.Query3_2_5;

			var reader = await selectCommand.ExecuteReaderAsync();
			var columns = reader.GetColumnSchema();
			CollectionAssert.AreEquivalent(new []{"CustomerID", "City"}, columns.Select(c => c.ColumnName));

			var type = new [] {new {CustomerID = "", City = ""}}.GetType();
			var expectedCustomers = (IEnumerable)JsonConvert.DeserializeObject(await File.ReadAllTextAsync(Path.Combine(TestContext.CurrentContext.TestDirectory, "Results", "Query3_2_5.json")), type);
			var customers = new List<dynamic>();
			while (await reader.ReadAsync())
			{
				var customerId = reader["CustomerID"].ToString();
				var city = reader["City"].ToString();
				customers.Add(new {CustomerID = customerId, City = city});
			}

			CollectionAssert.AreEquivalent(expectedCustomers, customers);
		}

		[Test]
		public async Task Query3_2_6()
		{
			var selectCommand = _dbConnection.CreateCommand();
			selectCommand.CommandText = Queries.Query3_2_6;

			var reader = await selectCommand.ExecuteReaderAsync();
			var columns = reader.GetColumnSchema();
			CollectionAssert.AreEquivalent(new []{"UserName", "Boss"}, columns.Select(c => c.ColumnName));

			var type = new [] {new {UserName = "", Boss = ""}}.GetType();
			var expectedEmployees = (IEnumerable)JsonConvert.DeserializeObject(await File.ReadAllTextAsync(Path.Combine(TestContext.CurrentContext.TestDirectory, "Results", "Query3_2_6.json")), type);
			var employees = new List<dynamic>();
			while (await reader.ReadAsync())
			{
				var userName = reader["UserName"].ToString();
				var boss = reader["Boss"].ToString();
				employees.Add(new {UserName = userName, Boss = boss});
			}

			CollectionAssert.AreEquivalent(expectedEmployees, employees);
		}

		[Test]
		public async Task Query4_1_1_1()
		{
			StringAssert.Contains("VALUES", Queries.Query4_1_1_1.ToUpper());

			var lastOrderIdSelectQuery = @"SELECT MAX(OrderID) FROM Orders";
			var selectCommand = _dbConnection.CreateCommand();
			selectCommand.CommandText = lastOrderIdSelectQuery;

			var lastOrderId = (int?)(long?) await selectCommand.ExecuteScalarAsync();

			var insertCommand = _dbConnection.CreateCommand();
			insertCommand.CommandText = Queries.Query4_1_1_1;
			await insertCommand.ExecuteNonQueryAsync();

			var insertedOrderId = (int?)(long?) await selectCommand.ExecuteScalarAsync();

			Assert.AreEqual(lastOrderId + 1, insertedOrderId);
		}

		[Test]
		public async Task Query4_1_1_2()
		{
			StringAssert.Contains("SELECT", Queries.Query4_1_1_2.ToUpper());
			StringAssert.DoesNotContain("VALUES", Queries.Query4_1_1_2.ToUpper());

			var lastOrderIdSelectQuery = @"SELECT MAX(OrderID) FROM Orders";
			var selectCommand = _dbConnection.CreateCommand();
			selectCommand.CommandText = lastOrderIdSelectQuery;

			var lastOrderId = (int?)(long?) await selectCommand.ExecuteScalarAsync();

			var insertCommand = _dbConnection.CreateCommand();
			insertCommand.CommandText = Queries.Query4_1_1_2;
			await insertCommand.ExecuteNonQueryAsync();

			var insertedOrderId = (int?)(long?) await selectCommand.ExecuteScalarAsync();

			Assert.AreEqual(lastOrderId + 1, insertedOrderId);
		}
	}
}